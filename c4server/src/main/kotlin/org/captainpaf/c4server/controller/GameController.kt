package org.captainpaf.c4server.controller


import org.captainpaf.c4server.domain.Color
import org.captainpaf.c4server.service.GameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class GameController @Autowired constructor(val gameService: GameService) {

        @GetMapping("/board")
        fun greeting(model : Model) : String {
            model.addAttribute("board", gameService.getBoard())
            if(gameService.getInfo().winner != null) {
               if(gameService.getInfo().winner == Color.RED) {
                   model.addAttribute("winner", "Les rouges gagnent")
               } else {
                   model.addAttribute("winner", "Les jaunes gagnent")
               }
            }
            return "board"
        }

}