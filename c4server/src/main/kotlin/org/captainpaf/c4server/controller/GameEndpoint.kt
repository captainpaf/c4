package org.captainpaf.c4server.controller

import org.captainpaf.c4server.domain.Color
import org.captainpaf.c4server.domain.GameInfo
import org.captainpaf.c4server.service.GameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.validation.constraints.Max
import javax.validation.constraints.Min

/**
 * api de gestion des parties
 */
@RestController
class GameEndpoint @Autowired constructor(val gameService: GameService) {

    @PostMapping("/game/play")
    fun play(@RequestParam(value = "column") @Min(1) @Max(7) column: Int,
             @RequestParam(value = "color") color: Color) {
        gameService.play(color, column - 1)
    }

    @GetMapping("/game")
    fun getGame() : GameInfo {
        return gameService.getInfo()
    }

}