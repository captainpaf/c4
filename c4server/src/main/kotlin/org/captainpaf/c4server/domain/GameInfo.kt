package org.captainpaf.c4server.domain

class GameInfo(val nextPlayer : Color, val winner : Color?, val board : Array<Array<Color?>>)