package org.captainpaf.c4server.domain

enum class Color {
    RED, YELLOW
}