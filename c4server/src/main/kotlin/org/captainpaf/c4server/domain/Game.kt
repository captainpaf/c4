package org.captainpaf.c4server.domain

import java.lang.Exception

class Game(val name: String)
{
    var lastColor = Color.YELLOW

    var winner : Color? = null

    val board = arrayOf(
        arrayOfNulls<Color>(7)
        , arrayOfNulls<Color>(7)
        , arrayOfNulls<Color>(7)
        , arrayOfNulls<Color>(7)
        , arrayOfNulls<Color>(7)
        , arrayOfNulls<Color>(7)
    )

    fun initBoard() {
        for (line in 0..5) {
            for (column in 0..6) {
                board[line][column] = null
            }
        }
    }

    private fun countDiag(column: Int, line: Int, color: Color?, coefColumn: Int, coefLine: Int) : Int
    {
        var lineTmp = line + coefLine
        var columnTmp = column + coefColumn

        var count = 0

        while( lineTmp >= 0 && lineTmp <= 5
            && columnTmp >=0 && columnTmp <= 6
            && board[lineTmp][columnTmp] == color)
        {
            if(board[lineTmp][columnTmp]==color)
            {
                count++
            }
            lineTmp += coefLine
            columnTmp += coefColumn
        }
        return count
    }

    private fun checkDiagAscWin(column: Int, line: Int, color: Color?) : Boolean
    {
        return (countDiag(column, line, color, 1, -1)
        + countDiag(column, line, color, -1, 1)) >= 3
    }

    private fun checkDiagDescWin(column: Int, line: Int, color: Color?) : Boolean
    {
        return (countDiag(column, line, color, 1, 1)
                + countDiag(column, line, color, -1, -1)) >= 3
    }

    private fun checkVerticalWin(column: Int, line: Int, color: Color?) : Boolean
    {
        if(line < 3) {
            return board[line+1][column] == color
                    && board[line+2][column] == color
                    && board[line+3][column] == color
        }
        return false
    }

    private fun checkHorizontalWin(column: Int, line: Int, color: Color?) : Boolean
    {
        return (countDiag(column, line, color, 1, 0)
                + countDiag(column, line, color, -1, 0)) >= 3
    }

    private fun setWinner(column : Int, line : Int)
    {
        val color = board[line][column]
        if(checkVerticalWin(column, line, color)) {
            winner = color
            return
        }

        if(checkHorizontalWin(column, line, color)) {
            winner = color
            return
        }

        if(checkDiagAscWin(column, line, color)) {
            winner = color
            return
        }

        if(checkDiagDescWin(column, line, color)) {
            winner = color
            return
        }

    }

    fun play(color : Color, column : Int)
    {
        // tester si la partie est finie
        if(winner != null)
        {
            throw Exception("Game is finished !")
        }

        // tester si c'est bien au joueur de jouer
        if(color == lastColor) {
            throw Exception("Not you turn")
        }

        // rechercher une case libre
        for (line in 0..5) {
            if(board[5-line][column] == null) {
                board[5-line][column] = color
                lastColor = color
                setWinner(column, 5-line)
                return
            }
        }

        // column is full
        throw Exception("Bad play")
    }

    fun info() : GameInfo {
        if(lastColor == Color.RED) {
            return GameInfo(Color.YELLOW, winner, board)
        }
        return GameInfo(Color.RED, winner, board)
    }
}