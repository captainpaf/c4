package org.captainpaf.c4server.service

import org.captainpaf.c4server.domain.Color
import org.captainpaf.c4server.domain.Game
import org.captainpaf.c4server.domain.GameInfo
import org.springframework.stereotype.Service

@Service
class GameService {

    private val game = Game("test")

    fun play(color : Color, column : Int) {
        return game.play(color, column)
    }

    fun getBoard() : Array<Array<Color?>> {
        return game.board
    }

    fun getInfo() : GameInfo {
        return game.info()
    }
}