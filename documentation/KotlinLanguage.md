# Le Langage Kotlin

## Présentation

Kotlin est un langage opensource qui a été créé par la société JetBrain. Le projet à commencé en 2010 afin de pallier à certains manques du langage Java tout en bénéficiant de son écosystème.

Il a la particularité d'être interropérable avec le language Java.

La première version à été publiée en 2016.

En 2019, google fait de Kotlin le language officiel pour développer sur sa plateforme android.

### Liens

- Site officiel : https://kotlinlang.org/docs/home.html
- Documentation en français : https://laurent-bernabe.developpez.com/tutoriels/java/introduction-langage-kotlin/
- Cours gratuits : https://www.coursera.org/learn/kotlin-for-java-developers
- Site pour tester le langage : https://play.kotlinlang.org
- Site pour héberger ses  projet : https://about.gitlab.com/

## Les outils

- Environnement java : https://www.oracle.com/java/technologies/downloads/#license-lightbox
- Environnement de développement (IDE) : Intellij : https://www.jetbrains.com/fr-fr/idea/
- Gestionnaire de code : Git : https://git-scm.com/, http://gitextensions.github.io/
- Outil http rest : https://www.soapui.org/

## Les bases du language

### Les variables

Les variables permettent d'identifier une zone en mémoire (RAM) pour stocker des informations.

En Kotlin, on distingue les variables dont on peut changer la valeur, et les variables immuables.

Kotlin étant un language typé, on doit préciser le type de variable après le nom.

Ex. :

```kotlin
val nom: String = "Toto"
// nom = "Dodo" // Interdit!!! Car nom a ete declare avec val
var age = 10
age += 12 // aucun probleme car age est alterable.
```

> Créer des variables nom, prenom et age

#### Null safety

Kotlin tente de régler un problème très connu de beaucoup de langages : le null pointeur exception.

Par défaut, les variables doivent contenir une valeur. Si on souhaite qu'une variable puisse être vide (ou nul), il faut explicitement le préciser avec le mot clé ?

```kotlin
var a:Int? = null
```

#### L'opérateur elvis

L'opérateur elvis permet d'assigner une valeur par défaut dans le cas ou une variable serait null.

```kotlin
val a: Int? = null
val b: Int = a ?: 0
// val b: Int = a // erreur car b ne peut pas être null

println(b)
```

### Les types de base

- **Byte** : nombre entier sur 1 byte
- **Short** : nombre entier sur 2 bytes
- **Int :** nombre entier sur 4 bytes
- **Long**: nombre entier  sur 8 bytes
- **Float** : nombre décimal sur 4 bytes
- **Double** : nombre décimal sur 8 bytes
- **String** : chaine de caractères
- **Boolean** : vrai ou faux

### Les fonctions

Les fonctions permettent de déclarer des blocs d'instructions qui pourront être joués plusieurs fois au sein d'un programme. En kotlin, les fonctions sont déclarées à l'aide du mot clé fun. Les fonctions peuvent ne pas retourner de valeurs et sont alors communément appelées procédures.

> Créer une fonction main()
>
> Créer une fonction hello(), avec et sans paramètres puis avec un retour

Fonction sans argument :

```kotlin
fun direBonjour()
{
   println("Hello world")
}
```

Fonction avec argument(s) :

```kotlin
fun direBonjour(nom: String, prenom: String)
{
   println("Bonjour ${nom} ${prenom} !")
}
```

Fonction qui retourne un résultat :

```kotlin
fun addition(numberOne: Int, numberTwo: Int) : Long
{
   return numberOne + numberTwo
}
```

#### La fonction main

La fonction main est particulière car c'est le point d'entré d'un programme. Elle sera la première fonction exécutée par le système.

```kotlin
fun main()
{
    println(addition(5, 8))
}
```

```kotlin
fun main(args: Array<String>)
{
    println("function main with command line arguments")
}
```

### Les collections natives

#### Les tableaux

Les tableaux permettent de stocker des valeurs du même type. Ils sont représenter en Kotlin par la classe généric Array. On peut construire un Array à l'aide du constructeur ou du mot clé arrayOf. Un tableau peut être multidimensionnel.

```kotlin
val tableau1 = arrayOf(2,10,-1,4,9)
val tableau2 = Array(10, { i -> i * 2 }) // utilisation d'une fonction lambda (fonction anonyme)
assert(tableau1[1] == 10) // On accede aux elements comme en Java
tableau1[0] = 170
assert(tableau1[0] == 170) // De meme pour la modification
val tableau3 = arrayOf(arrayOf(1,2,3), arrayOf(4,5,6)) // tableau multidimensionnel
```

> Créer un tableau contenant les chiffres de 0 à 9
>
> Créer un tableau contenant les chiffres de 99 à 0

#### Les listes

Les listes sont similaires aux tableaux unidimensionnels mais son plus puissantes. Avec kotlin, on distingue les listes modifiables (mutable) et les listes non modifiables. Les listes autorisent d'ajouter plusieurs fois le même élément.

```kotlin
val maListeNonModifiable = listOf(1,2,3)
// Interdit !!! maListeNonModifiable[2] = 6
// Interdit !!! maListeNonModifiable.add(10)
    
val maListeModifiable = mutableListOf(1,2,3)
maListeModifiable[2] = 6;
maListeModifiable += 10; // Equivalent a maListeModifiable.add(10)
    
print(maListeModifiable) // => [1, 2, 6, 10]
```

> Initialiser un liste avec des couleurs, ajouter une couleur. Puis afficher une couleur de cette liste au hazard

#### Les sets

Les sets ressemblent aux listes à la différence qu'ils n'autorisent pas les doublons.

```kotlin
val mesPiecesCapturees = setOf("Cavalier", "Dame", "Fou")
val mesPiecesCaptureesV2 = mesPiecesCapturees - "Dame"
    
val mesComics = mutableSetOf("Spiderman", "Batman", "Superman")
mesComics += "Green Lantern"
```

#### Les tables associatives (map)

Les tables associatives permettent de stocker des clés valeurs.

```kotlin
val monDictionnaireImmuable = mapOf("Sandra" to 27, "Alex" to 10)
val monDictionnaireModifiable = mutableMapOf("Sandra" to "0102030405", "Alex" to "0104050607")
    
monDictionnaireModifiable += ("Beatrice" to "0809101112")
monDictionnaireModifiable["Sandra"] = "0802030405"
val monDictionnaireImmuableEnrichi = monDictionnaireImmuable + ("Beatrice" to 30)
```

### Les conditions

les conditions permettent l' exécution de bloc d'instructions si certaines conditions sont réunies.

#### if, else if, else

```kotlin
if (1<2) {
    println("ok")
}
else if (2 != 2) {
    println("ko 1")
}
else {
    println("ko 2")
}
```

#### when

```kotlin
// exemple simple
when {
    a < 0 -> println("negatif")
    a == 0 -> println("nul")
}

// assignation d'une variable avec when
val a = 10;
var signeDeA: String =
when {
    a > 0 -> "positif"
    a == 0 -> "nul"
    else -> "negatif"
}
```

> Faire une fonction qui prend un nombre en paramètre et qui retourne vrai si le nombre est supérieur à 50.55

### Les boucles

Les boucles permettent exécuter plusieurs fois une portion de programme en faisant évoluer des variables.

#### while, do while

```kotlin
// afficher les chiffres avec while
var i = 0
while (i < 10) {
  println(i)
  i += 1    
}

// afficher les chiffres avec do while
var j = 0
do {
   println(j)
   j += 1    
} while (j < 10)
```

#### for

Le mot clé for permet de boucler sur des éléments itérables (des ranges ou certaines collections)

```kotlin
// afficher les chiffres
for (i in 0..9) println(i)

// afficher quelques nombres premiers
val monTableau = arrayOf(2,3,5,7,11,13)
for (premier in monTableau) {
    println("$premier est un nombre premier")
}

// afficher le tableau avec index
val monTableau = arrayOf(2,3,5,7,11,13)
for ((index, valeur) in monTableau.withIndex()) {
   println("$index: $valeur")
}

// afficher une table associative (map)
val monRepertoireTel = mapOf("Bea" to "0123456789", "Lily" to "08123456", "Max" to "06123456")
    
for (entree in monRepertoireTel) {
    println("${entree.key} => ${entree.value}")
}
```

> Faire une fonction qui prend en parametre un tableau d'Int et qui affiche l'ensemble du contenu du tableau

### Les classes

Les classes sont une spécificité des langages Object. Elles permettent de regrouper des variables et des fonctions au sein d'une même entité. Elle permettent de représenter des choses concrètes ou abstraites et de les utiliser dans le programme.

> Créer une classe Personne avec nom, prenom et age

Par exemple, on pourrait représenter une personne à l'aide de la classe suivante :

```kotlin
class Personne (val nom: String, val prenom: String, age: Int) {
    
    var age = age
        private set  
    
    fun vieillir() {
        age += 1
    }
}
```

Pour créer une personne particulière, nous devons instancier la classe Personne en utilisant son constructeur. L'instanciation consiste à stocker en mémoire un objet correspondant à une classe.

Ex : création du personnage Homer SIMPSON :

```kotlin
val homer = Personne("SIMPSON", "Homer", 40)
println(homer.age)
homer.vieillir()
println(homer.age)
```

Il y a beaucoup d'autres notions autour des classes : héritage, polymorphisme, interface... Ceci dépasse le cadre de cette simple initiation. N'hésitez pas à regarder les docs pour en savoir plus.

### Les enumerations

Les énumérations sont utilisées pour définir un ensemble fini d'éléments.
    

```kotlin
enum class Couleur {
    ROUGE, VERT, BLEU, JAUNE
}
```
