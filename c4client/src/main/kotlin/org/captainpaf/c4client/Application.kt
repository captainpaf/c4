package org.captainpaf.c4client

import kotlinx.coroutines.delay

class Application

    suspend fun main(args: Array<String>) {
        if(args.isEmpty()) {
            println("error, want 1 arg color")
            return
        }

        val color = Color.valueOf(args[0])

        val c4ApiClient = C4ApiClient()
        val engine = Engine()
        while (true) {
            val gameInfo = c4ApiClient.getGameInfo()
            if(gameInfo.winner != null) {
                return
            }
            if(gameInfo.nextPlayer == color) {
                val column = engine.bestMove(gameInfo.board) ?: return
                c4ApiClient.play(column = column, color)
            }
            delay(1000)
        }
    }