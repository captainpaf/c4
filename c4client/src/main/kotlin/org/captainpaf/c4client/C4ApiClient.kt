package org.captainpaf.c4client

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.delay


class C4ApiClient {

val client = HttpClient(CIO) {
    install(JsonFeature) {
        serializer = JacksonSerializer()
    }
}

suspend fun play(column : Int, color : Color) {
    return client.post("http://localhost:8080/game/play?column=$column&color=$color")
}

suspend fun getGameInfo() : GameInfo {
    return client.get("http://localhost:8080/game")
}

}
