package org.captainpaf.c4client

class Engine {

private fun getMoves(board : Array<Array<Color?>>) : MutableList<Int> {
    val moves = mutableListOf<Int>()
    for(column in 0..6) {
        if(board[0][column]==null) {
            moves.add(column+1);
        }
    }
    return moves
}

fun bestMove(board : Array<Array<Color?>>) : Int? {
    return getMoves(board).randomOrNull()
}
}